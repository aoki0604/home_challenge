#!/usr/bin/env python
# -*- coding: utf-8 -*-
# http://demura.net/lecture/12469.html
import rospy
import roslib
roslib.load_manifest('ros_start')
import sys
import cv2
import numpy as np
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

class image_converter:
    def __init__(self):
        self.Image_str = String()
        self.pub = rospy.Publisher("image_str", String, queue_size=10)
        self.bridge = CvBridge()
        self.sub = rospy.Subscriber("/usb_cam/image_raw",Image,self.callback)
        cv2.startWindowThread()

    def callback(self,data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8") #画像取得
        except CvBridgeError as e:
            print(e)
        hsv_image = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        lower_red = np.array([150,100,150])
        upper_red = np.array([180,255,255])
    
        lower_blue = np.array([110,50,50])
        upper_blue = np.array([130,255,255])
    
        red_mask = cv2.inRange(hsv_image, lower_red, upper_red);
        blue_mask = cv2.inRange(hsv_image, lower_blue, upper_blue);
    
        red_image  = cv2.bitwise_and(cv_image, cv_image, mask = red_mask)
        blue_image  = cv2.bitwise_and(cv_image, cv_image, mask = blue_mask)
        cv2.imshow('origenal',cv_image)
        cv2.imshow('red',red_image)
        cv2.imshow('blue',blue_image)
        cv2.waitKey(1)
        image, red_contours, hierarchy  = cv2.findContours(red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        image, blue_contours, hierarchy  = cv2.findContours(blue_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        red_area = [0]
        blue_area = [0]
        for i in range(len(red_contours)):
            red_area.append(cv2.contourArea(red_contours[i]))
        for i in range(len(blue_contours)):
            blue_area.append(cv2.contourArea(blue_contours[i]))
        red_area.sort() #赤面積昇順に並べ替え
        blue_area.sort() #青面積
        if red_area[-1] > blue_area[-1] and red_area[-1] > 500:
            self.Image_str.data = "Stop"
            self.pub.publish(self.Image_str)
            rospy.loginfo("I send " + self.Image_str.data)
        elif blue_area[-1] > red_area[-1] and blue_area[-1] > 500:
            self.Image_str.data = "Go"
            self.pub.publish(self.Image_str)
            rospy.loginfo("I send " + self.Image_str.data)

def main(args):
    ic = image_converter()
    rospy.init_node('image_converter')
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)
