#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from std_msgs.msg import String

rospy.init_node('talker')
pub = rospy.Publisher('chatter', String, queue_size=10) 
rate = rospy.Rate(10)
while not rospy.is_shutdown():
    fruit_str = String()
    a = raw_input("りんご、ぶどう、バナナ、みかん、もも、の内から一つ入力してください:")
    fruit_str.data = a
    pub.publish(fruit_str)
    rate.sleep()
