#!/usr/bin/env python
# -*- coding: utf-8 -*-
#以下参考
# https://andrewdai.co/xbox-controller-ros.html#rosjoy
# http://wiki.ros.org/ja/ROS/Tutorials/UnderstandingTopics
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from time import sleep

#flag = 0
def callback(message): 
    for i in range(2):
        twist = Twist()
        if i==0: #回転
            rospy.loginfo("指示 ：" + message.data)
            if message.data == "前": #方向回転無し
                twist.linear.x = 0.0
                twist.angular.z = 0.0
            elif message.data =="後":
                twist.linear.x = 0.0
                twist.angular.z = 3.1 #後ろ向く
            elif message.data == "左":
                twist.linear.x = 0.0
                twist.angular.z = 1.5 #左向く
            elif message.data == "右":
                twist.linear.x = 0.0
                twist.angular.z = 4.7 #右向く
            else:
                rospy.loginfo("その指示は指定されていません")
                return
            sleep(1)
        elif i==1: #前進
            twist.linear.x = 3.0
            twist.angular.z = 0.0
        pub.publish(twist) #送信
        #rate.sleep()
        sleep(1)

#rate = rospy.Rate(10)
rospy.Subscriber("kame_str", String, callback) #入力文字受け取る
pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
rospy.init_node('kame_move')
rospy.spin()

