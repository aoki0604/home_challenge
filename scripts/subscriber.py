#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
from std_msgs.msg import String

def callback(message): 
    #rospy.loginfo("I heard %s", message.data)
    
    
    if message.data in dict:
        rospy.loginfo(message.data + "の色は" + dict[message.data] + "です。")
    else:
        rospy.loginfo(message.data + "は辞書に登録されていません")

dict = {"りんご":"赤","ぶどう":"紫","バナナ":"黄","みかん":"オレンジ","もも":"ピンク"}
rospy.init_node('listener')
sub = rospy.Subscriber('chatter', String, callback)
rospy.spin()
