#!/usr/bin/env python
# -*- coding: utf-8 -*-
#以下参考
# https://andrewdai.co/xbox-controller-ros.html#rosjoy
# http://wiki.ros.org/ja/ROS/Tutorials/UnderstandingTopics
import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from time import sleep

def callback(message):
    rospy.loginfo("I hear " + message.data)
    twist = Twist()
    if message.data == "Go":
        twist.linear.x = 3.0
        twist.angular.z = 2.0
    elif message.data == "Stop":
        twist.linear.x = 0.0
    pub.publish(twist) #左回転
    #sleep(1)

rospy.Subscriber("image_str", String, callback) #入力文字受け取る
pub = rospy.Publisher('turtle1/cmd_vel', Twist, queue_size=10)
rospy.init_node('image_move')
rospy.spin()
